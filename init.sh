terraform init -reconfigure \
  -backend-config=bucket=testing-s3-backend \
  -backend-config=key=abc-staging/testproduct.tfstate \
  -backend-config=dynamodb_table=backend-state-locks \
  -backend-config=role_arn=arn:aws:iam::005752530308:role/TestS3BackendRole \
  -backend-config=region=ap-southeast-1 \
  -backend-config="access_key=${AWS_ACCESS_KEY_ID}" \
  -backend-config="secret_key=${AWS_SECRET_ACCESS_KEY}" \
  -input=false -no-color