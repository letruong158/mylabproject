# locals {
#     tags = {
#         Name = "Test-S3BackEnd"
#     }
# }

# data "aws_iam_policy_document" "policy_doc" {
#   statement {
#     actions   = ["s3:ListBucket"]
#     resources = [aws_s3_bucket.terraform_state.arn]
#   }

#   statement {
#     actions   = ["s3:GetObject", "s3:PutObject", "s3:DeleteObject"]
#     resources = ["${aws_s3_bucket.terraform_state.arn}/*"]
#   }

#   statement {
#     actions   = ["dynamodb:GetItem", "dynamodb:PutItem", "dynamodb:DeleteItem"]
#     resources = [aws_dynamodb_table.terraform_locks.arn]
#   }
# }

# resource "aws_iam_policy" "policy" {
#   name   = "${title(var.product)}S3BackendPolicy"
#   path   = "/"
#   policy = data.aws_iam_policy_document.policy_doc.json
# }

# resource "aws_iam_role" "iam_role" {
#   name = "${title(var.product)}S3BackendRole"

#   assume_role_policy = <<-EOF
#   {
#     "Version": "2012-10-17",
#     "Statement": [
#       {
#         "Action": "sts:AssumeRole",
#         "Principal": {
#         "AWS": ${jsonencode(local.principal_arns)}
#       },
#       "Effect": "Allow"
#       }
#     ]
#   }
#   EOF

#   tags = local.tags
# }

# resource "aws_iam_role_policy_attachment" "policy_attach" {
#   role       = aws_iam_role.iam_role.name
#   policy_arn = aws_iam_policy.policy.arn
# }

# # resource "aws_resourcegroups_group" "resourcegroups_group" {
# #   name = "${var.product}-s3-backend"

# #   resource_query {
# #     query = <<-JSON
# #       {
# #         "ResourceTypeFilters": [
# #           "AWS::AllSupported"
# #         ],
# #         "TagFilters": [
# #           {
# #             "Key": "project",
# #             "Values": ["${var.product}"]
# #           }
# #         ]
# #       }
# #     JSON
# #   }
# # }

# data "aws_caller_identity" "current" {}

# locals {
#   principal_arns = var.principal_arns != null ? var.principal_arns : [data.aws_caller_identity.current.arn]
# }

# output "config" {
#   value = {
#     bucket         = aws_s3_bucket.terraform_state.bucket
#     #region         = data.aws_region.current.name
#     role_arn       = aws_iam_role.iam_role.arn
#     dynamodb_table = aws_dynamodb_table.terraform_locks.name
#   }
# }