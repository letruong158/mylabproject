resource "aws_network_interface" "testWeb" {
  for_each  = var.subnet
  subnet_id = each.value.subnet_id
  private_ips = each.value.ip_address
  security_groups = ["sg-09bb158757f0e89ec"]
}

# data "aws_network_interface" "nic_id" {
#   id = values(aws_network_interface.testWeb)[*].id
# }

# resource "aws_instance" "testWeb" {
#   count                            = "3"
#   ami                              = "ami-04ff9e9b51c1f62ca"
#   instance_type                    = "t2.small"
#   key_name                         = "gitlab-runner-keypair"
#   # iam_instance_profile             = "EC2AccessS3"
#   root_block_device {
#     volume_size                    = "8"
#     volume_type                    = "gp2"
#     # iops                           = "10000"
#     delete_on_termination          = "false"
#   }
#   tags = {
#     Name                           = "testWeb-0${count.index + 1}p"
#     Environment                    = "Test"
#     App                            = "App"
#     Function                       = "App"
#   }
#   network_interface {
#     network_interface_id = each.value.nic_id
#     device_index = 0
#   }
# }


# resource "aws_network_interface" "testWeb" {
#   count = "3"
#   subnet_id = "${element(var.subnet,count.index)}"
#   # subnet_id = ["${lookup(var.subnet,count.index)}"]
#   private_ips = ["${lookup(var.ips,count.index)}"]
#   security_groups = ["sg-09bb158757f0e89ec"]
# }

resource "aws_instance" "testWeb" {
  count                            = "2"
  ami                              = "ami-04ff9e9b51c1f62ca"
  instance_type                    = "t2.small"
  key_name                         = "gitlab-runner-keypair"
  # iam_instance_profile             = "EC2AccessS3"
  root_block_device {
    volume_size                    = "8"
    volume_type                    = "gp2"
    # iops                           = "10000"
    delete_on_termination          = "false"
  }
  tags = {
    Name                           = "testWeb-0${count.index + 1}p"
    Environment                    = "Test"
    App                            = "App"
    Function                       = "App"
  }
  network_interface {
    network_interface_id = aws_network_interface.testWeb[count.index].id
    device_index = 0
  }
}
