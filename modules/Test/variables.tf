variable "subnet" {
  type = map(any)
  default = {
     0 = {
        subnet_id        = "subnet-07f45fdf75039d7ee"
        ip_address       = ["10.10.1.20"]
    },
     1 = {
        subnet_id        = "subnet-05852851165c5c1fc"
        ip_address       = ["10.10.2.20"]
    }, 
}
}


# variable "ips" {
#   type = map(string)
#   default = {
#       "0" = "10.10.1.20"
#       "1" = "10.10.2.20"
#       "2" = "10.10.1.21"
#   }
# }

# variable "subnet" {
#   type = list(string)
#   default = ["subnet-07f45fdf75039d7ee","subnet-05852851165c5c1fc"]
#   }

    # default = {
  #     "0" = "subnet-07f45fdf75039d7ee"
  #     "1" = "subnet-05852851165c5c1fc"