provider "aws" {
  region  = "ap-southeast-1"
}




# variable "region" {
#   description = "The aws region. https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html"
#   type        = string
#   default     = "us-east-1"
# }

variable "principal_arns" {
  description = "A list of principal arns allowed to assume the IAM role"
  default     = null
  type        = list(string)
}

variable "product" {
  description = "Product-Environment"
  default     = "Test"
  type        = string
}

# variable "availability_zones_count" {
#   description = "The number of AZs."
#   type        = number
#   default     = 3
# }

# variable "vpc_cidr_block" {
#   type    = string
#   default = "10.1.0.0/16"
# }


# variable "namespace" {
#   type    = string
#   default = "eks-testing"
# }

# variable "subnet_cidr_bits" {
#   description = "The number of subnet bits for the CIDR. For example, specifying a value 8 for this parameter will create a CIDR with a mask of /24."
#   type        = number
#   default     = 8
# }

# variable "zone_offset" {
#   type        = number
#   description = "CIDR block bits extension offset to calculate Public subnets, avoiding collisions with Private subnets. If we have 3 AZs, here we will set to 3"               
#   default     = 3
# }

# variable "cluster_name" {
#   type        = string
#   default     = "EKS-cluster-custom"
# }

# variable "karpenter_namespace" {
#   description = "The K8S namespace to deploy Karpenter into"
#   default     = "karpenter"
# # }

# variable "cluster_oidc_url" {
#   description = "The OIDC Issuer URL for the cluster"
#   default     = "https://oidc.eks.us-east-1.amazonaws.com/id/51E1BBF3F14384278784A712829814A0"
# }

# variable "cluster_endpoint" {
#   description = "The API endpoint for the cluster"
#   default     = "https://51E1BBF3F14384278784A712829814A0.gr7.us-east-1.eks.amazonaws.com"
# }