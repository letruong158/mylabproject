# resource "aws_s3_bucket" "terraform_state" {
#   bucket = "testing-s3-backend"
#   force_destroy = false 
  
#   tags = {
#     Name = "s3-backend-state"
#   }
# }

# resource "aws_s3_bucket_public_access_block" "s3_bucket" {
#   bucket = aws_s3_bucket.terraform_state.id

#   block_public_acls       = true
#   block_public_policy     = true
#   ignore_public_acls      = true
#   restrict_public_buckets = true
# }


# # resource "aws_s3_bucket_acl" "s3_bucket" {
# #   bucket  = aws_s3_bucket.terraform_state.id
# #   acl     = "private"
# # }

# resource "aws_s3_bucket_versioning" "s3_version" {
#   bucket  = aws_s3_bucket.terraform_state.id

#   versioning_configuration {
#     status  = "Enabled"
#   }
# }

# # resource "aws_kms_key" "kms_key" {
# #   tags = {
# #     Name = "kms-backend"
# #   }
# # }

# # resource "aws_s3_bucket_server_side_encryption_configuration" "s3_bucket" {
# #   bucket = aws_s3_bucket.terraform_state.id

# #   rule {
# #     apply_server_side_encryption_by_default {
# #       sse_algorithm     = "aws:kms"
# #       kms_master_key_id = aws_kms_key.kms_key.arn
# #     }
# #   }
# # }

# #===============================================================

# resource "aws_dynamodb_table" "terraform_locks" {
#   name         = "backend-state-locks"
#   billing_mode = "PAY_PER_REQUEST"
#   hash_key     = "LockID"
#   attribute {
#     name = "LockID"
#     type = "S"
#   }

#   tags = {
#     Name = "s3-locking-state"
#   }
# }

