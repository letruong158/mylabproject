terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  # backend "http" {} 
}
resource "aws_iam_role" "role" {
  name         = "EC2-Access-S3"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_policy" {
    policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
    role       = aws_iam_role.role.name
}
resource "aws_s3_bucket" "tttttesttttt" {
  bucket = "tttttesttttt"
  force_destroy = false 
  tags = {
    Name = "tttttesttttt"
  }
}

resource "aws_s3_bucket_public_access_block" "test" {
  bucket = aws_s3_bucket.tttttesttttt.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}



#Running S3 backend after creating S3 bucket completed
terraform {
  backend "s3" {
    bucket         = "testing-s3-backend"
    dynamodb_table = "backend-state-locks"
    key            = "abc-staging/testproduct.tfstate"
    region         = "ap-southeast-1"
    encrypt        = false
    role_arn       = "arn:aws:iam::005752530308:role/TestS3BackendRole"
  }
}




# module "psgTest" {
#   source = "./modules/Test"
# }




# resource "aws_network_interface" "Test-EC2-01" {
#   subnet_id   = "subnet-09d4164b1feb0bae7"
#   # private_ips = ["10.0.1.5"]
#   tags = {
#     Name = "my-nic-01"
#   }
# }


# resource "aws_instance" "Test-EC2" {
#   ami           = "ami-0c802847a7dd848c0"
#   instance_type = "t2.nano"
#   availability_zone = "ap-southeast-1c"
#   tags = {
#     Name = "Test-EC2Test"
#   }
#   network_interface {
#     network_interface_id = aws_network_interface.Test-EC2-01.id
#     device_index = 0
#   }
# }